<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'bx_sitemap_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?>







<?//var_dump($arResult['SECTIONS']);?>

<?if($arResult['SECTIONS'] and (int)$arResult['SECTIONS'][0]['DEPTH_LEVEL']==1):?>

	<?$i = 0;?>
	<div class="wrapp_content align-items" data-ah-role="block">
		<?foreach($arResult['SECTIONS'] as $key => $val):?>
		<?//print_r($val)?>
		<div class="js_shop_list shop_list2 <?($i==0 || $i==4)?'first-child':''?>" data-ah-role="item" style="display: block; height: <?$i<5?'1064':'394'?>px;">
			<a href="<?=$val['SECTION_PAGE_URL']?>" class="name_cat" data-ah-role="name" style="height: 30px;"><?=$val['NAME']?> <span class="count"> [<?=CIBlockSection::GetCount(Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"],"SECTION_ID"=>$val['ID']))?>]</span></a>
			<ul class="list_cat">
			
			<?
			$arFilter = Array(
			'IBLOCK_ID'=>$arParams["IBLOCK_ID"], 
			'GLOBAL_ACTIVE'=>'Y', 
			//'SECTION_ID'=>$arResult['SECTION_ID']);
			'SECTION_ID'=>$val['ID']);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
		while($ar_result = $db_list->GetNext())
		{
			$rsSection = CIBlockSection::GetList(array(), array('ID' => $ar_result['ID'], 'ELEMENT_SUBSECTIONS' => 'N'), true, array());
			?><li class="shop_cat_link2"><a href="<?=$ar_result['SECTION_PAGE_URL']?>"><?=$ar_result['NAME']?> <?if ($arSection = $rsSection->GetNext()) {echo "[".$arSection['ELEMENT_CNT']."]";}?></a></li><?
			//print_r($ar_result);
			
		}?>
			
			
			
			
			
				
			</ul>
			<a href="<?=$val['SECTION_PAGE_URL']?>" class="all_tov">все товары</a>
		</div>
		<?$i++?>
		<?endforeach;?>
	</div>

<?endif;?>





<?if($arResult['SECTIONS'] and (int)$arResult['SECTIONS'][0]['DEPTH_LEVEL']==2):?>

<h1><?=$arResult['SECTION']['NAME']?></h1>






<?/*<pre><?print_r($arResult)?></pre>*/?>
	<?foreach($arResult['SECTIONS'] as $key => $val):?>
	<div class="js_shop_list shop_list">
		<details class="products-details">
			<summary class="shop_cat_link">
				<span><?=$val['NAME']?></span>
				<a class="seemore" href="<?=$val['SECTION_PAGE_URL']?>"><?=$val['ELEMENT_CNT']?></a>
			</summary>
			<table class="tabletov">
				<tbody>
					<tr>
						<th class="first"></th>
						<th class="second">Артикул</th>
						<th class="name_c">Наименование</th>
						<th class="edin">Ед. Изм.</th>
						<th class="cena">Цена (с НДС в руб.)</th>
						<th class="colich">Колличество</th>
						<th class="last"></th>
					</tr>
					<?
					$my_slider = CIBlockElement::GetList (
					Array("ID" => "ASC"),
					Array("IBLOCK_ID" => 6, 'SECTION_ID'=>(int)$val['ID']),
					false,
					Array("nPageSize"=>10),
					Array('ID','NAME','DETAIL_PAGE_URL', "PROPERTY_ARTICLE", "PROPERTY_EI", "PREVIEW_PICTURE")
					);
					$io = 1;
					while($ar_fields = $my_slider->GetNext()) {?>
					<tr>
						<td style="text-align: center;"><?=$io;$io++;?></td>
						<td><?=$ar_fields['PROPERTY_ARTICLE_VALUE']?></td>
						<td>
							<a href="<?=$ar_fields['DETAIL_PAGE_URL']?>" class="shop-item-title"><?=$ar_fields['NAME']?></a>
						</td>
						<td class="edin"><?=$ar_fields['PROPERTY_EI_VALUE']?></td>
						<td class="row_bay" colspan="3">
							<form method="post" action="" class="js_shop_form shop_form ajax">
								<input type="hidden" name="good_id" value="<?=$ar_fields['PROPERTY_ARTICLE_VALUE']?>">
								<input type="hidden" name="module" value="shop">
								<input type="hidden" name="action" value="">
								
								<input type="hidden" name="IDT" value="<?=$ar_fields['ID']?>">	
								<input type="hidden" name="PRICET" value="<?=CPrice::GetBasePrice($ar_fields['ID'])['PRICE']?>">
								<input type="hidden" name="NAME" value="<?=$ar_fields['NAME']?>">
								<input type="hidden" name="DETAIL" value="<?=$ar_fields['DETAIL_PAGE_URL']?>">
								<input type="hidden" name="PIC" value="<?=CFile::GetPath((int)$ar_fields['PREVIEW_PICTURE'])?>">
								<?//=print_r($ar_fields)?>
								<table>
									<tbody>
										<tr>
											<td class="cena"><?=CPrice::GetBasePrice($ar_fields['ID'])['PRICE']?> руб.</td>
											<td class="colich">
												<input type="text" value="1" name="count" class="number" pattern="[0-9]+([\.|,][0-9]+)?" step="any">
											</td>
											<td class="sub-bay">
												<input type="button" class="button on_kup solid" value="Купить" action="buy">
												<div class="wrapp_bg_all"></div>
												<div class="block_cupit_wrapp">
													<div class="wrapp_sub">
														<p>Товар добавлен. Для оформления перейдите в корзину</p>
														<div class="cupit_i_cart cup_sub">Перейти в корзину</div> 
														<div class="otmena cup_sub">Продолжить покупки</div>
													</div>	
												</div> 
											</td>
										</tr>
									</tbody>
								</table>
							</form>
						</td>
					</tr>
				<?
			}
			?>
				</tbody>
			</table>			
			<div>
				<a href="<?=$val['DETAIL_PAGE_URL']?>" class="show_more">Показать все товары (всего <?=$val['ELEMENT_CNT']?>)</a>
			</div>
		</details>
		

		
		
		
		
		
	</div>
	<?endforeach;?>
	
	
	
		<?
		if ($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y')
		{
			echo $arResult['SECTION']['DESCRIPTION'];
		}
		?>
<div class="clear"></div>
<?endif;?>


<?/*

<div class="<? echo $arCurView['CONT']; ?>"><?
if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
{
	$this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

	?><h1
		class="<? echo $arCurView['TITLE']; ?>"
		id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>"
	><a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>"><?
		echo (
			isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
			? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
			: $arResult['SECTION']['NAME']
		);
	?></a></h1><?
}
if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<ul class="<? echo $arCurView['LIST']; ?>">
<?
	switch ($arParams['VIEW_MODE'])
	{
		case 'LINE':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if (false === $arSection['PICTURE'])
					$arSection['PICTURE'] = array(
						'SRC' => $arCurView['EMPTY_IMG'],
						'ALT' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							: $arSection["NAME"]
						),
						'TITLE' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							: $arSection["NAME"]
						)
					);
				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
				<a
					href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
					class="bx_catalog_line_img"
					style="background-image: url('<? echo $arSection['PICTURE']['SRC']; ?>');"
					title="<? echo $arSection['PICTURE']['TITLE']; ?>"
				></a>
				<h2 class="bx_catalog_line_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
				}
				?></h2><?
				if ('' != $arSection['DESCRIPTION'])
				{
					?><p class="bx_catalog_line_description"><? echo $arSection['DESCRIPTION']; ?></p><?
				}
				?><div style="clear: both;"></div>
				</li><?
			}
			unset($arSection);
			break;
		case 'TEXT':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><h2 class="bx_catalog_text_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
				}
				?></h2></li><?
			}
			unset($arSection);
			break;
		case 'TILE':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if (false === $arSection['PICTURE'])
					$arSection['PICTURE'] = array(
						'SRC' => $arCurView['EMPTY_IMG'],
						'ALT' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							: $arSection["NAME"]
						),
						'TITLE' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							: $arSection["NAME"]
						)
					);
				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
				<a
					href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
					class="bx_catalog_tile_img"
					style="background-image:url('<? echo $arSection['PICTURE']['SRC']; ?>');"
					title="<? echo $arSection['PICTURE']['TITLE']; ?>"
					> </a><?
				if ('Y' != $arParams['HIDE_SECTION_NAME'])
				{
					?><h2 class="bx_catalog_tile_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
					if ($arParams["COUNT_ELEMENTS"])
					{
						?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
					}
				?></h2><?
				}
				?></li><?
			}
			unset($arSection);
			break;
		case 'LIST':
			$intCurrentDepth = 1;
			$boolFirst = true;
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
				{
					if (0 < $intCurrentDepth)
						echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']),'<ul>';
				}
				elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
				{
					if (!$boolFirst)
						echo '</li>';
				}
				else
				{
					while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
					{
						echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
						$intCurrentDepth--;
					}
					echo str_repeat("\t", $intCurrentDepth-1),'</li>';
				}

				echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
				?><li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><h2 class="bx_sitemap_li_title"><a href="<? echo $arSection["SECTION_PAGE_URL"]; ?>"><? echo $arSection["NAME"];?><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection["ELEMENT_CNT"]; ?>)</span><?
				}
				?></a></h2><?

				$intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
				$boolFirst = false;
			}
			unset($arSection);
			while ($intCurrentDepth > 1)
			{
				echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
				$intCurrentDepth--;
			}
			if ($intCurrentDepth > 0)
			{
				echo '</li>',"\n";
			}
			break;
	}
?>
</ul>
<?
	echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
}
?></div>
*/?>