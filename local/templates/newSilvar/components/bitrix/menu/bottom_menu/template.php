<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;
?>
<?
//print_r($arResult);
?>
<?/*
<ul id="top-menu">
	<li class=" li29">
		<a href="http://www.silvar.ru/press-tsentr/">Пресс-центр</a>
	</li>
	<li class=" li30">
		<a href="http://www.silvar.ru/rekvizity/">Реквизиты</a>
	</li>
	<li class=" li31">
		<a href="http://www.silvar.ru/o-kompanii/">О компании</a>
	</li>
</ul>

die();
*/
?>

<ul id="top-menu">
	<?foreach($arResult as $itemIdex => $arItem):?>
		<?if ($arItem["DEPTH_LEVEL"] == "1"):?>
			<li class="li29"><a href="<?=htmlspecialcharsbx($arItem["LINK"])?>"><?=htmlspecialcharsbx($arItem["TEXT"])?></a></li>
		<?endif?>
	<?endforeach;?>
</ul>

<?/*
<nav class="bx-inclinksfooter-container">
	<ul class="bx-inclinksfooter-list">
		<?foreach($arResult as $itemIdex => $arItem):?>
			<?if ($arItem["DEPTH_LEVEL"] == "1"):?>
				<li class="bx-inclinksfooter-item"><a href="<?=htmlspecialcharsbx($arItem["LINK"])?>"><?=htmlspecialcharsbx($arItem["TEXT"])?></a></li>
			<?endif?>
		<?endforeach;?>
	</ul>
</nav>
*/?>