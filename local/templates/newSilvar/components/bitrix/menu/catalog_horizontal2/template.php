<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();



//print_r($arResult);

/*
?>
<div class="wrapp_site">
	<a href="<?=SITE_TEMPLATE_PATH?>/s1/spets-predlozheniya/">
		<div class="predloj">лучшие предложения</div>
	</a>
	<a href="<?=SITE_TEMPLATE_PATH?>/s1/otpravit-zayavku/">
		<div class="zajavka">отправить заявку с файлом</div>
	</a>
	<a href="<?=SITE_TEMPLATE_PATH?>/s1/korzina/">
		<div class="cart_iso">выбрано товаров  (<span id="co_ca">0</span>)</div>
	</a>
	<div class="scrol_to">наверх</div>
</div>

<?
*/


$ico = Array('','predloj','zajavka','cart_iso');
?>


<div class="wrapp_site">
	<?foreach($arResult['ALL_ITEMS'] as $key => $val) {?>
		<a href="<?=PRE_URL?><?=$val['LINK']?>">
			<div class="<?=next($ico)?>">
				<?=$val['TEXT']?>
				<?if((int)$key == 1430168220) {echo "(<span id=\"co_ca\">0</span>)";}?>
				<?//print_r($key)?>
			</div>
		</a>
	
	<?}?>
	<div class="scrol_to">наверх</div>
</div>













