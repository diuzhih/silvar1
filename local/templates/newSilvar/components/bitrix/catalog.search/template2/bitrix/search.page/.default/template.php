<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<div class="search-page">
	<div class="top_search">
		<form action="/search/index.php" class="js_search_form search_form" method="get" id="search">
			<div class="search-input search-input1">
				<input type="hidden" name="module" value="search">
				<input type="submit" class="submitW" value="Искать">
				<input id="textbox" type="text" name="q" class="textboxW" value="<?=$arResult["REQUEST"]["QUERY"]?>" placeholder="Поиск" />
			</div>
		</form>
	</div>
</div>