(function(k) {
    !window.addEventListener && function(c, b, e, f, a, h, g) {
        c[f] = b[f] = e[f] = function(c, b) {
            var a = this;
            g.unshift([a, c, b, function(d) {
                d.currentTarget = a;
                d.preventDefault = function() {
                    d.returnValue = !1
                };
                d.stopPropagation = function() {
                    d.cancelBubble = !0
                };
                d.target = d.srcElement || a;
                b.call(a, d)
            }]);
            this.attachEvent("on" + c, g[0][3])
        };
        c[a] = b[a] = e[a] = function(a, c) {
            for (var b = 0, d; d = g[b]; ++b)
                if (d[0] == this && d[1] == a && d[2] == c) return this.detachEvent("on" + a, g.splice(b, 1)[0][3])
        };
        c[h] = b[h] = e[h] = function(a) {
            return this.fireEvent("on" +
                a.type, a)
        }
    }(Window.prototype, HTMLDocument.prototype, Element.prototype, "addEventListener", "removeEventListener", "dispatchEvent", []);
    window.dGoals = function(c, b, e) {
        "function" == typeof b && (e = b, b = "click");
        if ("string" == typeof c) {
            var f = c;
            c = function(a) {
                return k(a.target).is(f)
            }
        }
        document.addEventListener(b, function(a) {
            a.target && c(a) && e()
        }, !0)
    }
})(jQuery);

$('body').on('click', '.meil', function(){
  yaCounter36220615.reachGoal('tel-mail');
  ga('send', 'pageview', '/tel-mail');
});

dGoals('.zajavka', 'click', function() {
  yaCounter36220615.reachGoal('otpravka-zayavki');
  ga('send', 'pageview', '/otpravka-zayavki');
});

$('body').on('click', '.button.on_kup.solid', function(){
  yaCounter36220615.reachGoal('v-korzinu');
  ga('send', 'pageview', '/v-korzinu');
});

$('body').on('click', '.button.solid.sub_on', function(){
  yaCounter36220615.reachGoal('v-korzinu');
  ga('send', 'pageview', '/v-korzinu');
});

$('body').on('submit', '.js_registration_form.registration_form.ajax', function(){
  yaCounter36220615.reachGoal('otpr-registraciya');
  ga('send', 'pageview', '/otpr-registraciya');
});

dGoals('.vhod', 'click', function() {
  yaCounter36220615.reachGoal('vhod');
  ga('send', 'pageview', '/vhod');
});

$('body').on('submit', '.login', function(){
  yaCounter36220615.reachGoal('otpravit-vhod');
  ga('send', 'pageview', '/otpravit-vhod');
});

dGoals('.callBack', 'click', function() {
  yaCounter36220615.reachGoal('forma-zvonok');
  ga('send', 'pageview', '/forma-zvonok');
});

$('body').on('submit', '.feedback_form form.ajax', function(){
  yaCounter36220615.reachGoal('otpravit-zvonok');
  ga('send', 'pageview', '/otpravit-zvonok');
});