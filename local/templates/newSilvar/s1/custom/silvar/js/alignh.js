;(function($) {
	// ah-role="root"
	var alignHeight = function(node) {
		this.node = node;
		this.krole = null;
		this.steps = [];
		this.elements = {};
		
		this.init();
	}

	alignHeight.prototype.init = function() {
		var i;
		this.map(this.node, function(parent, current) {
			var role = current.getAttribute('data-ah-role'), n;
			if(role && (role != 'block')) {
				if(typeof(this.elements[role]) == 'undefined')
					this.elements[role] = [];
				n = this.elements[role].length;
				if((n == 1) && !this.krole)
					if(this.elements[role][0].parentNode == current.parentNode)
						this.krole = role;	
				this.elements[role][n] = current;
			}
		});
		
		if(!this.krole)
		for(i in this.elements) {
			this.krole = i;
			break;
		}
	}
	
	alignHeight.prototype.map = function(parent, callback) {
		var i, child = parent.childNodes;
		for(i = 0; i < child.length; i++) 
		if(child[i].nodeType == 1) {
			callback.apply(this, [parent, child[i]]);
			this.map(child[i], callback);
		}
	}
	alignHeight.prototype.reset = function() {
		var items = this.elements[this.krole], 
			i, step = -1, n = items.length;

		this.steps = [];
		for(i = 0; i < n; i++)
		{
			items[i].style.display = 'block';
			items[i].style.height = '60px';
			items[i].className = items[i].className.replace(' first-child', '');
			
			if((i == 0) ||
				(items[i-1].offsetTop<items[i].offsetTop)) {
				//items[i].style.marginLeft = 0;
				items[i].className += ' first-child';
				if(i > 0) {
					step = this.steps.length;
					this.steps[step] = i;
				}	
			}
			
		}
		if((step<0) || (this.steps[step] != n))
			this.steps[step+1] = n;
	}
	
	alignHeight.prototype.setHeight = function(only) {
		var items, item, step, maxHeight, iFrom, r, j, k, pos;
		only = only || false;
		for(r in this.elements) 
		{
			items = this.elements[r];
			if(only && (only != r)) continue;
			iFrom = 0;
			for(j = 0; j < this.steps.length; j++)
			{
				step = this.steps[j];
				maxHeight = 0;
				for(k = iFrom; k < step; k++)
				{
					item = items[k];
					item.style.height = 'auto';
					if(item.offsetHeight > maxHeight)
						maxHeight = item.offsetHeight;
				}
				if(step - iFrom > 1) 
				for(k = iFrom; k < step; k++) {
					item = items[k];
					item.style.height = maxHeight+'px';
				}
				iFrom = step;
			}
		}
	}
	
	alignHeight.prototype.align = function()
	{
		if(this.krole) {
			this.reset(); 
			this.setHeight(); 
			this.setHeight(this.krole);
		}
	}

	$.fn.Align = function() 
	{
		return this.each(function() {
			var object = new alignHeight(this);
			$(this).bind('align', function() {
				object.align();
			});
		});
	}
	
	
})(jQuery);

$(function() {
	$('.align-items').Align();
	$(window).resize(function () { $('.align-items').trigger('align'); });
});

$(window).bind('load', function(e) {  
	$('.align-items').trigger('align');
});