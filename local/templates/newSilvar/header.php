<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
CJSCore::Init(array("fx"));
$curPage = $APPLICATION->GetCurPage(true);
$theme = COption::GetOptionString("main", "wizard_eshop_bootstrap_theme_id", "blue", SITE_ID);
?>




<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<!--link rel="stylesheet" href="<?=PRE_URL?>/cache/css/19251df2c043b46c5ee3e663208e0f77.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8"-->
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?=PRE_URL?>/news/rss/">
	<!--title>Расходные материалы для монтажа от компании «Сильвар»</title-->
	<meta name="description" content="Купить расходные материалы для монтажа от компании «Сильвар». Мы предлагаем большой ассортимент расходников для монтажа по низким ценам и индивидуальный подход к каждому клиенту. Доставка товаров по всей России.">
	<meta name="keywords" content="silvar">
	<link rel="icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/favicon.ico?v=2" />





	<?
	$APPLICATION->ShowHead();
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/colors.css", true);
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/bootstrap.css");
	$APPLICATION->SetAdditionalCSS("/bitrix/css/main/font-awesome.css");
	
	
	
	
	//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/s1/custom/silvar/css/style.css?v=1", true);
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/s1/custom/silvar/css/default.css?v=1", true);
	
	?>
	
	<title><?$APPLICATION->ShowTitle()?></title>
	
	
	<link rel="stylesheet" href="http://silvar1.bd8.ru<?=SITE_TEMPLATE_PATH?>/s1/custom/silvar/css/style.css?v=1" type="text/css" media="screen">
	
	
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<meta charset="utf-8">
<meta content="Russian" name="language">
<meta content="DiAfan http://www.diafan.ru/" name="author">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="yandex-verification" content="29c64e6649143033" />
<meta name="google-site-verification" content="kJXtBePvMHQOyBjVpEA4WSxHC45s8Grrkxg-uJrhqYk" />

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101222634-1', 'auto');
  ga('send', 'pageview');
</script>
<style>
.sum_cart_info{white-space: nowrap;}
</style>

</head>
<?
global $USER;


?>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
	<div class="wrapp_search">
		<div class="wrapp_site">
		
			<div class="top_search">
			
				<?$APPLICATION->IncludeComponent(
					"bitrix:catalog.search", 
					"template1", 
					array(
						"ACTION_VARIABLE" => "action",
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"BASKET_URL" => "/personal/cart/",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
						"CHECK_DATES" => "N",
						"CONVERT_CURRENCY" => "N",
						"DETAIL_URL" => "",
						"DISPLAY_BOTTOM_PAGER" => "Y",
						"DISPLAY_COMPARE" => "N",
						"DISPLAY_TOP_PAGER" => "N",
						"ELEMENT_SORT_FIELD" => "sort",
						"ELEMENT_SORT_FIELD2" => "id",
						"ELEMENT_SORT_ORDER" => "asc",
						"ELEMENT_SORT_ORDER2" => "desc",
						"HIDE_NOT_AVAILABLE" => "N",
						"HIDE_NOT_AVAILABLE_OFFERS" => "N",
						"IBLOCK_ID" => "",
						"IBLOCK_TYPE" => "catalog",
						"LINE_ELEMENT_COUNT" => "3",
						"NO_WORD_LOGIC" => "N",
						"OFFERS_LIMIT" => "5",
						"PAGER_DESC_NUMBERING" => "N",
						"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
						"PAGER_SHOW_ALL" => "N",
						"PAGER_SHOW_ALWAYS" => "N",
						"PAGER_TEMPLATE" => ".default",
						"PAGER_TITLE" => "Товары",
						"PAGE_ELEMENT_COUNT" => "30",
						"PRICE_CODE" => array(
						),
						"PRICE_VAT_INCLUDE" => "Y",
						"PRODUCT_ID_VARIABLE" => "id",
						"PRODUCT_PROPERTIES" => array(
						),
						"PRODUCT_PROPS_VARIABLE" => "prop",
						"PRODUCT_QUANTITY_VARIABLE" => "quantity",
						"PROPERTY_CODE" => array(
							0 => "",
							1 => "",
						),
						"RESTART" => "N",
						"SECTION_ID_VARIABLE" => "SECTION_ID",
						"SECTION_URL" => "",
						"SHOW_PRICE_COUNT" => "1",
						"USE_LANGUAGE_GUESS" => "Y",
						"USE_PRICE_COUNT" => "N",
						"USE_PRODUCT_QUANTITY" => "N",
						"COMPONENT_TEMPLATE" => "template1"
					),
					false
				);?>
			</div>
			<noindex>
				<div class="alfavit">
					<div class="title">по алфавиту</div>
					<div class="alfavit_rus">
						<a href="/catalog/letter.php?letter=А">А</a> 
						<a href="/catalog/letter.php?letter=Б">Б</a> 
						<a href="/catalog/letter.php?letter=В">В</a> 
						<a href="/catalog/letter.php?letter=Г">Г</a> 
						<a href="/catalog/letter.php?letter=Д">Д</a> 
						<a href="/catalog/letter.php?letter=Е">Е</a> 
						<a href="/catalog/letter.php?letter=Ж">Ж</a> 
						<a href="/catalog/letter.php?letter=З">З</a> 
						<a href="/catalog/letter.php?letter=И">И</a> 
						<a href="/catalog/letter.php?letter=К">К</a> 
						<a href="/catalog/letter.php?letter=Л">Л</a> 
						<a href="/catalog/letter.php?letter=М">М</a> 
						<a href="/catalog/letter.php?letter=Н">Н</a> 
						<a href="/catalog/letter.php?letter=О">О</a> 
						<a href="/catalog/letter.php?letter=П">П</a> 
						<a href="/catalog/letter.php?letter=Р">Р</a> 
						<a href="/catalog/letter.php?letter=С">С</a> 
						<a href="/catalog/letter.php?letter=Т">Т</a> 
						<a href="/catalog/letter.php?letter=У">У</a> 
						<a href="/catalog/letter.php?letter=Ф">Ф</a> 
						<a href="/catalog/letter.php?letter=Ц">Ц</a> 
						<a href="/catalog/letter.php?letter=Ч">Ч</a> 
						<a href="/catalog/letter.php?letter=Ш">Ш</a> 
						<a href="/catalog/letter.php?letter=Щ">Щ</a> 
						<a href="/catalog/letter.php?letter=Э">Э</a> 
						<a href="/catalog/letter.php?letter=Ю">Ю</a> 
						<a href="/catalog/letter.php?letter=Я">Я</a> <br/>
						<a href="/catalog/letter.php?letter=A">A</a> 
						<a href="/catalog/letter.php?letter=B">B</a> 
						<a href="/catalog/letter.php?letter=C">C</a> 
						<a href="/catalog/letter.php?letter=D">D</a> 
						<a href="/catalog/letter.php?letter=E">E</a> 
						<a href="/catalog/letter.php?letter=F">F</a> 
						<a href="/catalog/letter.php?letter=G">G</a> 
						<a href="/catalog/letter.php?letter=I">I</a> 
						<a href="/catalog/letter.php?letter=J">J</a> 
						<a href="/catalog/letter.php?letter=K">K</a> 
						<a href="/catalog/letter.php?letter=L">L</a> 
						<a href="/catalog/letter.php?letter=M">M</a> 
						<a href="/catalog/letter.php?letter=N">N</a> 
						<a href="/catalog/letter.php?letter=O">O</a> 
						<a href="/catalog/letter.php?letter=P">P</a> 
						<a href="/catalog/letter.php?letter=Q">Q</a> 
						<a href="/catalog/letter.php?letter=R">R</a> 
						<a href="/catalog/letter.php?letter=S">S</a> 
						<a href="/catalog/letter.php?letter=T">T</a> 
						<a href="/catalog/letter.php?letter=U">U</a> 
						<a href="/catalog/letter.php?letter=V">V</a> 
						<a href="/catalog/letter.php?letter=W">W</a> 
						<a href="/catalog/letter.php?letter=X">X</a> 
						<a href="/catalog/letter.php?letter=Y">Y</a> 
						<a href="/catalog/letter.php?letter=Z">Z</a> 
					</div>
				</div>
			</noindex>
		</div>
	</div>

<div  class="wrapp_log_cart">
<div class="wrapp_site">
<div class="wrapp_right_c">
<ul class="regist">
	<li>
		<?if($USER->IsAuthorized()) {?>
			<a class="registr" href="<?=PRE_URL?>/personal/private/">Профиль</a>
		<?} else {?>
			<a class="registr" href="<?=PRE_URL?>/login/registratsiya.php">Pегистрация</a>
		<?}?>
		
		
		
		
	</li>
	<li>
		<?if($USER->IsAuthorized()) {?>
			<a class="vhod" href="<?=PRE_URL?>/personal/private/?logout=yes&bitrix_include_areas=N&clear_cache=Y" class="logaut">Выход</a>
		<?} else {?>
			<span class="vhod" onClick="window.location.href='<?=PRE_URL?>/auth/'">Вход</span>
		<?}?>
	</li>
</ul>
	




<?
CModule::IncludeModule("sale");
$arBasket = GetBasketList();
$arResult["TOTALPRICE"] = 0;
foreach($arBasket as $arBasketItem) {
	$arResult["TOTALPRICE"] += $arBasketItem["PRICE"] * $arBasketItem["QUANTITY"];
}
?>	
<span class="cart_block top-line-item">
	<span id="show_cart" class="js_show_cart">
		<a href="<?=PRE_URL?>/personal/cart/">
			<img class="img_cart" src="<?=PRE_URL.SITE_TEMPLATE_PATH?>/s1/custom/silvar/img/icon_cart.png" alt="">
			<span class="wr_cart">
				<span class="title_cart">Корзина пуста</span>
				<span class="sum_cart_info"><?=$arResult["TOTALPRICE"]?> руб.<span class="button"></span>
				</span>
			</span>
		</a>
		<span class="cart_open"></span>
		<form action="" method="POST" class="js_cart_block_form cart_block_form ajax">
			<input type="hidden" name="module" value="cart">
			<input type="hidden" name="action" value="recalc">
			<input type="hidden" name="form_tag" value="cart_block_form">
			<?
$arID = array();
$arBasketItems = array();
$dbBasketItems = CSaleBasket::GetList(
    array(
        "NAME" => "ASC",
        "ID" => "ASC"
    ),
    array(
        "FUSER_ID" => CSaleBasket::GetBasketUserID(),
        "LID" => SITE_ID,
        "ORDER_ID" => "NULL"
    ),
    false,
    false,
    array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "PRICE", "NAME","PREVIEW_PICTURE","DETAIL_PAGE_URL", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
);




?>
<table class="cart" cellspacing="0">
	<tbody>
<?
while ($arItems = $dbBasketItems->Fetch()) {

	if (CModule::IncludeModule("iblock")):
		$iblock_id = 6;
		$my_elements = CIBlockElement::GetList (
			Array("ID" => "ASC"),
			Array("IBLOCK_ID" => $iblock_id, "ID" => (int)$arItems['PRODUCT_ID']),
			false,
			false,
			Array('ID', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'DETAIL_PICTURE')
		);
		while($ar_fields = $my_elements->GetNext())
		{
			$img_path = CFile::GetPath($ar_fields["PREVIEW_PICTURE"]);
		}
	endif;

	//print_r($img_path);	
	//print_r($arItems);	

	
	

?>
				<tr>
					<td class="cart_img">
						<a href="<?=$arItems['DETAIL_PAGE_URL']?>">
							<img src="<?=$img_path?>" width="177" height="180" alt="<?=$arItems['NAME']?>" title="<?=$arItems['NAME']?>">
						</a>
					</td>
					<td colspan="3" class="cart_name">
						<a href="<?=$arItems['DETAIL_PAGE_URL']?>"><?=$arItems['NAME']?></a>
					</td>
					<td colspan="2" class="cart_summ"><?=$arItems['PRICE']?> руб.</td>
				</tr>
<?

}
	
?>
				<tr class="cart_last_tr">
					<td class="cart_total" colspan="4">Итого за товары</td>
					<td class="cart_summ" colspan="2"><?=$arResult["TOTALPRICE"]?> руб.</td> 
				</tr>
				</tbody>
			</table>
			<div class="errors error" style="display:none"></div>
		</form>
	</span>
</span>
</div>
</div>
</div>

<div class="wrapp_site hedear">

	<a href="<?=PRE_URL?>" class="logo">
		<img src="<?=PRE_URL.SITE_TEMPLATE_PATH?>/s1/custom/silvar/img/logo.png" alt="" title="" />
	</a>
	
	
	
<div class="wrapp_hed_cont wrapp_hed_cont1">
	<p class="meil"><a href="mailto:zakaz@silvar.ru"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer/mail.php"), false);?></a></p>
	<a href="tel:+74955438783"><p class="phone"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer/t1.php"), false);?></p></a>
	<a href="tel:+74957838794"><p class="phone"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer/t2.php"), false);?></p></a>
	<div class="top_search">
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.search", 
		"template2", 
		array(
			"ACTION_VARIABLE" => "action",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"BASKET_URL" => "/personal/cart/",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "N",
			"CONVERT_CURRENCY" => "N",
			"DETAIL_URL" => "",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_COMPARE" => "N",
			"DISPLAY_TOP_PAGER" => "N",
			"ELEMENT_SORT_FIELD" => "sort",
			"ELEMENT_SORT_FIELD2" => "id",
			"ELEMENT_SORT_ORDER" => "asc",
			"ELEMENT_SORT_ORDER2" => "desc",
			"HIDE_NOT_AVAILABLE" => "N",
			"HIDE_NOT_AVAILABLE_OFFERS" => "N",
			"IBLOCK_ID" => "",
			"IBLOCK_TYPE" => "catalog",
			"LINE_ELEMENT_COUNT" => "3",
			"NO_WORD_LOGIC" => "N",
			"OFFERS_LIMIT" => "5",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Товары",
			"PAGE_ELEMENT_COUNT" => "30",
			"PRICE_CODE" => array(
			),
			"PRICE_VAT_INCLUDE" => "Y",
			"PRODUCT_ID_VARIABLE" => "id",
			"PRODUCT_PROPERTIES" => array(
			),
			"PRODUCT_PROPS_VARIABLE" => "prop",
			"PRODUCT_QUANTITY_VARIABLE" => "quantity",
			"PROPERTY_CODE" => array(
				0 => "",
				1 => "",
			),
			"RESTART" => "N",
			"SECTION_ID_VARIABLE" => "SECTION_ID",
			"SECTION_URL" => "",
			"SHOW_PRICE_COUNT" => "1",
			"USE_LANGUAGE_GUESS" => "Y",
			"USE_PRICE_COUNT" => "N",
			"USE_PRODUCT_QUANTITY" => "N",
			"COMPONENT_TEMPLATE" => "template1"
		),
		false
	);?>
	</div>
	
	 <?$APPLICATION->IncludeComponent(
	"altasib:feedback.form", 
	"template2", 
	array(
		"ACTIVE_ELEMENT" => "Y",
		"ADD_HREF_LINK" => "Y",
		"ALX_LINK_POPUP" => "Y",
		"ALX_LOAD_PAGE" => "N",
		"ALX_NAME_LINK" => "Заказать звонок",
		"BBC_MAIL" => "",
		"CAPTCHA_TYPE" => "default",
		"CATEGORY_SELECT_NAME" => "Выберите категорию",
		"CHANGE_CAPTCHA" => "N",
		"CHECKBOX_TYPE" => "CHECKBOX",
		"CHECK_ERROR" => "Y",
		"COLOR_OTHER" => "#009688",
		"COLOR_SCHEME" => "BRIGHT",
		"COLOR_THEME" => "",
		"EVENT_TYPE" => "ALX_FEEDBACK_FORM",
		"FB_TEXT_NAME" => "",
		"FB_TEXT_SOURCE" => "PREVIEW_TEXT",
		"FORM_ID" => "1",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "altasib_feedback",
		"INPUT_APPEARENCE" => array(
			0 => "DEFAULT",
		),
		"JQUERY_EN" => "jquery",
		"LINK_SEND_MORE_TEXT" => "Отправить ещё одно сообщение",
		"LOCAL_REDIRECT_ENABLE" => "N",
		"MASKED_INPUT_PHONE" => array(
		),
		"MESSAGE_OK" => "Ваше сообщение было успешно отправлено",
		"NAME_ELEMENT" => "ALX_DATE",
		"NOT_CAPTCHA_AUTH" => "Y",
		"POPUP_ANIMATION" => "0",
		"PROPERTY_FIELDS" => array(
			0 => "PHONE",
			1 => "FIO",
			2 => "FEEDBACK_TEXT",
		),
		"PROPERTY_FIELDS_REQUIRED" => array(
			0 => "FEEDBACK_TEXT",
		),
		"PROPS_AUTOCOMPLETE_EMAIL" => array(
		),
		"PROPS_AUTOCOMPLETE_NAME" => array(
		),
		"PROPS_AUTOCOMPLETE_PERSONAL_PHONE" => array(
		),
		"PROPS_AUTOCOMPLETE_VETO" => "N",
		"SECTION_FIELDS_ENABLE" => "N",
		"SECTION_MAIL_ALL" => "zakaz@silvar.ru",
		"SEND_IMMEDIATE" => "Y",
		"SEND_MAIL" => "N",
		"SHOW_LINK_TO_SEND_MORE" => "Y",
		"SHOW_MESSAGE_LINK" => "Y",
		"USERMAIL_FROM" => "N",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_INPUT_LABEL" => "",
		"USER_CONSENT_IS_CHECKED" => "Y",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_CAPTCHA" => "Y",
		"WIDTH_FORM" => "50%",
		"COMPONENT_TEMPLATE" => "template2"
	),
	false
);?>
	
	
	
	
	<?/*<a href="" class="callBack">Заказать звонок</a>*/?>
</div>
	
	
	
	
	
	
	
	<?/*
	<div class="wrapp_hed_cont">
		<p class="meil">
			<a href="mailto:zakaz@silvar.ru">zakaz@silvar.ru</a>
		</p>
		<a href="tel:+74955438783"><p class="phone">+7 (495) 543-87-83</p></a>
		<a href="tel:+74957838794"><p class="phone">+7 (495) 783-87-94</p></a>
		
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter36220615 = new Ya.Metrika({
							id:36220615,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true,
							webvisor:true
						});
					} catch(e) { }
				});
		
				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";
		
				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript>
			<div>
				<img src="https://mc.yandex.ru/watch/36220615" style="position:absolute; left:-9999px;" alt="" />
			</div>
		</noscript>
	</div>
	*/?>
	
	
	

</div>

<div class="wrapp_menu">
<div class="top_menu">


<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"catalog_horizontal1", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "3",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "catalog_horizontal1"
	),
	false
);?>

</div>
</div>







<div class="wrapp_site" style="padding-left: 11px;">
<div class="wrapp_content">


<div class="breadcrumb1" xmlns:v="http://rdf.data-vocabulary.org/#">
	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "", array(
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "-"
			),
			false,
			Array('HIDE_ICONS' => 'Y')
		);?>
</div>







