<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ПРАЙС-ЛИСТ");
?>

<style>
ul, ol {
    color: #5a5a5a;
    margin-bottom: 15px;
    list-style-type: disc;
    margin-left: 30px !important;
    line-height: 18px;
    font-size: 14px;
}
</style>

<h1>Прайс-лист</h1>

<ul>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a href="/files/speckabel.xls"></a><a target="_blank" href="/files/speckabel.xls">Спецкабель</a><a target="_blank" href="/files/speckabel.xls"></a></span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/Hyperline%201.02.17.xls">HYPERLINE</a></span><br>
 <span style="font-size: 12pt;"> </span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/ostec.xls">Ostec</a></span><br>
 <span style="font-size: 12pt;"> </span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/Экопласт.xls">Экопласт</a></span><br>
 <span style="font-size: 12pt;"> </span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/legrand.xls">Legrand</a></span><br>
 <span style="font-size: 12pt;"> </span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/рексант%2022.05.2017.xls">Рексант</a></span><br>
 <span style="font-size: 12pt;"> </span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/АВВ_28.03.17.xls">АВВ</a></span><br>
 <span style="font-size: 12pt;"> </span></li>
	<li style="color: #000000;"><span style="font-size: 12pt;"><a target="_blank" href="/files/ДКС%2011.04.17.xls">DKC</a></span><br>
 </li>
</ul>
<span style="color: #000000;"> </span><br>
 <span style="font-size: 12pt;">Уважаемые коллеги!</span><br>
 <span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;">
Обращаем ваше внимание, что прайс-листы, размещенные в этом разделе, содержат РРЦ (рекомендуемые розничные цены) производителей.</span><br>
 <span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;">
Они могут понадобиться для расчета смет/проектов и для понимания порядка цены на товар.</span><br>
 <span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;">
Отпускные цены у нас - ДЕШЕВЛЕ</span><br>
 <span style="font-size: 12pt;"> </span><br>
 <span style="font-size: 12pt;">
подробнее у менеджеров.</span><br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>