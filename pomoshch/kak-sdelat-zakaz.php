<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как сделать заказ");
?>

<style>
ul, ol {
    color: #5a5a5a;
    margin-bottom: 15px;
    list-style-type: disc;
    margin-left: 30px !important;
    line-height: 18px;
    font-size: 14px;
}
</style>


<h1>Как сделать заказ</h1><p><strong>Заказ можно сделать любым удобным способом:</strong></p>
<ul>
<li>Через Корзину нашего сайта</li>
<li>Воспользоваться формой отправки заказа</li>
</ul>
<p><a href="http://www.silvar.ru/otpravit-zayavku/">http://www.silvar.ru/otpravit-zayavku/</a></p>
<ul>
<li>написать на почту, ICQ нашим менеджерам</li>
<li>позвонить</li>
<li>заказать звонок и мы сами Вам перезвоним.</li>
</ul>
<div class="clear"></div>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>